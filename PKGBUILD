# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Helmut Stult
# Contributor: graysky <therealgraysky AT protonmail DOT com>
# Contributor: germar <germar DOT reitze AT gmail DOT com>

pkgname=('backintime' 'backintime-qt')
pkgbase=backintime
pkgver=1.5.3
pkgrel=2
pkgdesc="Simple backup system inspired from the Flyback Project and TimeVault."
arch=('any')
url="https://github.com/bit-team/backintime"
license=('GPL-2.0-or-later')
makedepends=(
  'git'
  'python'
)
checkdepends=(
  'cronie'
  'openssh'
  'python-dbus'
  'python-keyring'
  'python-pyfakefs'
  'rsync'
  'systemd'
)
source=("git+https://github.com/bit-team/backintime.git#tag=v$pkgver")
sha256sums=('10a8e0fb9c37967d565e4f9db4a5afcda03ad10cf0cf46b0e4d5aa0c96272722')

prepare() {
  cd "$pkgbase"

  # Fix documentation directories.
  sed -i -e "s|$pkgbase-common|$pkgbase|g" \
    common/config.py common/configure common/tools.py
}

build() {
  cd "$pkgbase"

  pushd common
  ./configure --python
  popd

  pushd qt
  ./configure --python
  popd

  make -C common
  make -C qt
}

#check() {
#  cd "$pkgbase"

  # Set HOME to isolate some tests, which accesses files in ~/.ssh/, etc.
#  HOME=tmp make -C common test-v
  # Skip test_tools.py unit test
#  HOME=tmp pytest -sv -m "not test_tools.py"
#}

package_backintime() {
  depends=(
    'cron'
    'fuse2'
    'openssh'
    'python-dbus'
    'python-keyring'
    'python-packaging'
    'rsync'
  )
  optdepends=(
    'backintime-qt: Qt frontend'
    'encfs: encrypted filesystem in user-space'
#   'pm-utils: for laptops allows an option to not snapshot on battery'  # AUR
    'sshfs: FUSE client based on the ssh file transfer protocol'
  )
  conflicts=("$pkgbase-cli")
  replaces=("$pkgbase-cli")

  cd "$pkgbase"
  make -C common DESTDIR="$pkgdir" install

  # Manually compile Python bytecode
  python -m compileall -d / "$pkgdir/usr/share/$pkgbase/common/"
  python -O -m compileall -d / "$pkgdir/usr/share/$pkgbase/common/"
}

package_backintime-qt() {
  pkgdesc="Qt frontend for Back In Time"
  depends=(
    "backintime=$pkgver"
    'libnotify'
    'polkit'
    'python-dbus'
    'python-pyqt6'
    'xorg-xdpyinfo'
  )
  optdepends=(
    'gnome-keyring: option for SSH key storage'
    'kompare: option for comparing files'
    'kwallet: option for SSH key storage'
    'meld: option for comparing files'
    'oxygen-icons: fallback in case of missing icons'
    'python-secretstorage: option for SSH key storage'
  )

  cd "$pkgbase"
  make -C qt DESTDIR="$pkgdir" install

  # Manually compile Python bytecode
  python -m compileall -d / "$pkgdir/usr/share/$pkgbase/qt/"
  python -O -m compileall -d / "$pkgdir/usr/share/$pkgbase/qt/"
}
